define(['views/alertsView',
		'core'
	],
	function (AlertsView, homeRegions) {

		return {
			load: function (params) {

				mv.i.views.alertsView = new AlertsView({
					'el': mv.sections.mainContainer
				});

				mv.i.views.alertsView.render();
			}
		};

	});
