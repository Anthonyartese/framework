define(['views/bindingView',
		'views/boundView',
		'text!html/regions/tplTwoColumns.html',
		'core'
	],
	function (BindingView, BoundView, homeRegions) {

		return {
			load: function (params) {

				$(mv.sections.mainContainer).html(homeRegions);

				mv.i.views.bindingView = new BindingView({
					'el': '#col1'
				});

				mv.i.views.boundView = new BoundView({
					'el': '#col2',
					'model': mv.i.views.bindingView.model
				});

				mv.i.views.bindingView.render();
				mv.i.views.boundView.render();
			}
		};

	});
