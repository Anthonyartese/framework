/**
 * This file is an example of a presenter that is showing multiple views within different regions
 * of the page at the same time.  This is a different approach than having parent-child view
 * relationships
 */
define(['views/linksView',
		'views/alertsView',
		'text!html/regions/tplStacked.html',
		'core'
	],
	function (LinksView, AlertsView, homeRegions) {

		return {
			load: function (params) {

				$(mv.sections.mainContainer).html(homeRegions);

				mv.i.views.linksView = new LinksView({
					'el': '#region-links'
				});
				mv.i.views.alertsView = new AlertsView({
					'el': '#region-alerts'
				});

				mv.i.views.linksView.render();
				mv.i.views.alertsView.render();
			}
		};

	});
