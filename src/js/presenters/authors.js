define(['text!html/regions/tplAuthors.html',
		'core'
	],
	function (authorsRegions) {

		return {
			load: function (params) {

				$(mv.sections.mainContainer).html(authorsRegions);
			}
		};

	});
