define(['views/linksView',
		'core'
	],
	function (LinksView) {

		return {
			load: function (params) {

				mv.i.views.linksView = new LinksView({
					'el': mv.sections.mainContainer
				});

				mv.i.views.linksView.render();
			}
		};

	});
