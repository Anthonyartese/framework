define([
	'views/landingView',
	'text!html/regions/tplLanding.html',
	'core'
], function (LandingView, template) {

	return {
			load: function (params) {
				$(mv.sections.mainContainer).html(template);
				var view1 = 'landingView';
				mv.i.views[view1] = new LandingView({
					'el': '#landingGreeting'
					//model: asset
				});
				mv.i.views[view1].render();
			}
	};
});
