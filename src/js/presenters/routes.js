define([
	'views/routesView',
	'core'
], function (RoutesView) {

	return {
			load: function (params) {

				mv.i.views.routesView = new RoutesView({
					'el': mv.sections.mainContainer
				});

				mv.i.views.routesView.setup();
			}
		};

});
