/**
 * This file is meant to contain the core of the namespacing variables and functions
 * that are critical to the application.  This file is helpful in sharing required information
 * between the application and the unit testing setup.  Use this file to abstract environment-related
 * information
 */

define([], function () {

	// Setup global namespacing
	window.mv = {
		views: {},
		models: {},
		collections: {},
		i: { //for instaciated objects
			views: {},
			router: null
		},
		sections: {
			mainContainer: '#main-container'
		},
		enums: {
			alertTypes: {
				SUCCESS: 'Success',
				WARNING: 'Warning',
				DANGER: 'Danger'
			},
			envNames: {
				PROD: 'prod',
				QA: 'qa',
				TEST: 'test',
				LOCAL: 'local'
			},
			logTypes: {
				INFO: 'info',
				WARN: 'warn',
				ERROR: 'err'
			}
		},
		env: {
			name: '',
			logTypesToLog: [],
			logTypesToPersist: []
		},
		urls: {
			logging: '/logging'
		}
	};

	/**
	 * Meant to set environment specific variables or settings
	 * @param {string} hostString This can be either the environment descriptor or a url
	 */
	mv.env.setEnvironment = function (hostString) {
		switch (hostString) {
		case 'produrl':
		case 'prod':
			mv.env.name = mv.enums.envNames.PROD;
			mv.env.logTypesToLog = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN];
			mv.env.logTypesToPersist = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN];
			break;
		case 'qaurl':
		case 'qa':
			mv.env.name = mv.enums.envNames.QA;
			mv.env.logTypesToLog = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN, mv.enums.logTypes.INFO];
			mv.env.logTypesToPersist = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN];
			break;
		case 'testurl':
		case 'test':
			mv.env.name = mv.enums.envNames.TEST;
			mv.env.logTypesToLog = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN, mv.enums.logTypes.INFO];
			mv.env.logTypesToPersist = [];
			break;
		default:
			mv.env.name = mv.enums.envNames.LOCAL;
			mv.env.logTypesToLog = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN, mv.enums.logTypes.INFO];
			mv.env.logTypesToPersist = [];
		}
	};

	mv.env.setEnvironment(window.location.host);

	/**
	 * A function to aid in memory management to avoid zombie views.  It checks to see if view exists,
	 * if so, it will trigger the prototyped close method
	 * @param  {Object} view Backbone view (potential) instance
	 */
	mv.closeView = function (view) {
		if (view) {
			view.close();
		}
	};

});
