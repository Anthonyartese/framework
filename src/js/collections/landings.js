define([
	'models/landing',
	'core'
], function (Landing) {

	return Backbone.Collection.extend({

		initialize: function () {},

		model: Landing

	});

});
