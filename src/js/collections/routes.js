define([
	'models/routes',
	'json!mocks/routes.json',
/*	'collections/apiproxy.php',*/
	'core'
], function (Routes, mockRoutes) {

	return Backbone.Collection.extend({

		initialize: function () {},

		model: Routes,
		
		fetch: function () {			
				this.set(mockRoutes);
			},

/*		url: function () {
			return '/apiproxy.php';
		},
		*/

	});

});
