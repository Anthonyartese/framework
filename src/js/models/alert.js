define([
	'core'
], function () {

	return Backbone.Model.extend({

		// Explicitly listing what attributes to expect in this model
		defaults: {
			type: '',
			message: '',
			icon: '',
			scheme: ''
		},

		initialize: function () {}

	});

});
