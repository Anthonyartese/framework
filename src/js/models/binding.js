define([
	'core'
], function () {

	return Backbone.Model.extend({

		defaults: {
			fName: '',
			lName: '',
			email: '',
			state: '',
			gender: '',
			sendUpdates: ''
		},

		/** Define the validation rules and messages for the attributes in this model that should be validated
		 * Shows examples of multiple validation rules and custom error messages as well
		 */
		validation: {
			fName: {
				required: true
			},
			lName: {
				required: true,
				msg: 'Last name must be entered'
			},
			email: [{
				required: true
			}, {
				pattern: /^((?!abc).)*$/,
				msg: 'Its easy as 123'
			}, {
				pattern: 'email'
			}],
			state: {
				required: true
			},
			gender: {
				required: true
			}
		},

		initialize: function () {}

	});

});
