// RequireJS configuration
require.config({
	paths: {
		//paths
		html: '../html',
		lib: '../lib',
		mocks: '../mocks',
		test: '../../test',
		locale: '../locale',

		//requirejs plugins
		text: '../bower_components/requirejs-text/text',
		json: '../bower_components/requirejs-json/json',
		i18n: '../bower_components/requirejs-i18n/i18n',

		//Bower dependencies
		jquery: '../bower_components/jquery/dist/jquery',
		underscore: '../bower_components/underscore/underscore',
		backbone: '../bower_components/backbone/backbone',
		bootstrap: '../bower_components/bootstrap/dist/js/bootstrap',
		modelBinder: '../bower_components/backbone.modelbinder/Backbone.ModelBinder',
		validation: '../bower_components/backbone.validation/dist/backbone-validation-amd',

		//other libs and components
		backboneGlobal: '../lib/backbone.global-0.1.2'
	},
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		bootstrap: {
			deps: ['jquery']
		}
	}
});

// Define jQuery as AMD module
define.amd.jQuery = true;

// bootstrap the application
require([
	'router',
	'views/alertView',
	'core',
	'util/logger'
], function (Router, AlertView) {

	mv.i.router = new Router();
	mv.i.views.alertView = new AlertView();
	Backbone.history.start({
		pushState: true
	});
});
