define([
	'text!html/tplLandingGreeting.html',
	'core'
], function (template) {

	return Backbone.View.extend({

		el: '',

		template: _.template(template),

		initialize: function () {
			this.render();
		},

		render: function () {
			this.$el.html(this.template({}));
		},

		events: {
			"click #routesButton": "goToRoutesPage"
		},

		goToRoutesPage: function() {
			mv.i.router.navigate('/routes', {trigger : true});
		}

	});

});
