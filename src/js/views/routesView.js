define([
	'text!html/tplRoutes.html',
	'text!html/tplRoute.html',
	'collections/routes',
	'models/routes',
	'core'
], function (template, tplRoute, Routes, Route) {

	return Backbone.View.extend({

		el: '',

		template: _.template(template),

		initialize: function () {
			this.collection = new Routes();
		},

		setup: function () {
			var self = this;
			$.when(self.collection.fetch())
				.done(function () {
					console.log(self.collection.toJSON());
					self.render();
				})
				.fail(function (response) {
					console.log(response);
					console.log('request for data has failed');
				});
		},

		render: function () {
		var data = {
			collection: this.collection.toJSON()
		};	

		this.$el.html(_.template(template, data));
	
		},

		events: {
			"click #search": "searchRoutes",
			"click #searchAll": "listAllRoutes"
		},

		searchRoutes: function (event) {
			event.preventDefault();
			var self = this;
			var searchRoute = this.$('#searchInput').val();			
/*		var routeInfo =_.where(this.collection.toJSON(), {
				rt:searchRoute || rtnm:searchRoute
		})*/	

		var routeInfo = _.filter(this.collection.toJSON(), function(route) { 
			return route.rt == searchRoute || route.rtnm == searchRoute; 
		}); 

		var routeNum = routeInfo[0].rt;

		var routeModel = this.collection.find(function(model) { return model.get('rt') == routeNum ; })
				data = routeModel.toJSON();
		console.log(data);

		//mv.i.router.navigate('/routes' + data, {trigger : true});
		this.$el.html(_.template(tplRoute, data));		

		},
		listAllRoutes: function() {
			this.render();
		}

	});

});
