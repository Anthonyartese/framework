/**
 * This view is an example of how to use bi-directional data binding.  This example uses one of the
 * libs found here https://github.com/theironcook/Backbone.ModelBinder.  The page at that url has
 * example of how to instanciate, and different ways to bind DOM elements to data models
 */
define([
	'models/binding',
	'text!html/tplBinding.html',
	'modelBinder',
	'validation',
	'core'
], function (BindingModel, template, ModelBinder, Validation) {

	return Backbone.View.extend({

		el: '',

		//template: _.template(template),

		initialize: function () {
			this.model = new BindingModel();
			this.modelBinder = new ModelBinder();
		},

		render: function () {
			var that = this;
			this.$el.html(template);

			/**
			 * This is a mapping hash used to explicitly define the bi-directional mapping.  The
			 * key is the data model attribute, and the value is the DOM selector
			 */
			this.bindings = {
				'fName': '#fname',
				'lName': '#lname',
				'email': '#email',
				'state': '#state',
				'gender': '[name=gender]',
				'sendUpdates': '#send-updates'
			};

			/**
			 * Model binding options can be overridden on an instance or on a global level.  Here is an example
			 * where we modify the default events to when we desire it to happen
			 */
			var overriddenBinderOptions = {
				changeTriggers: {
					'': 'change',
					'[contenteditable]': 'blur',
					'#email': 'keyup'
				}
			};

			/**
			 * This binds the model to the view.
			 * Optional 3rd param is to provide explicit mappings.  Default will match element's "name" attribute to model attribute
			 * Optional 4rd param is for overriding global modelbinder options on an instance
			 */
			this.modelBinder.bind(this.model, this.el, this.bindings, overriddenBinderOptions);

			/**
			 * On change of the model, the attributes that have been changed should be validated
			 */
			this.model.bind('change', function () {
				that.model.validate(that.model.changedAttributes());
			});

			/**
			 * This binds the validation logic defined in the model to this view so changes to the model will be validated
			 * On success and failure, this will use the binding mapping to find the field on the screen to manipulate as necesarry
			 */
			Backbone.Validation.bind(this, {
				forceUpdate: true,
				valid: function (view, attr, selector) {
					var validField = that.$el.find(that.bindings[attr]);
					validField.closest('.form-group').addClass('has-success').removeClass('has-error');
					validField.siblings('.help-block').addClass('hidden').text('');
				},
				invalid: function (view, attr, error, selector) {
					var invalidField = that.$el.find(that.bindings[attr]);
					invalidField.closest('.form-group').addClass('has-error').removeClass('has-success');
					invalidField.siblings('.help-block').removeClass('hidden').text(error);
				}
			});
		},

		events: {
			'click #update-model': 'updateModel'
		},

		/**
		 * This function will update the data model directly, which in turn should
		 * update the view because of the plugin used in this view
		 * @param  {Object} event An actual click event
		 */
		updateModel: function (event) {
			event.preventDefault();
			this.model.set({
				'fName': 'Chris',
				'lName': 'Thorne',
				'email': 'cthorne@iwillbreakyou.info',
				'state': 'MN',
				'gender': 'male',
				'sendUpdates': true
			});
			this.model.validate();
		},

		/**
		 * This method is triggered via the prototyped close function if this this view is closed
		 * with something like viewInstance.close()
		 */
		onClose: function () {
			this.modelBinder.unbind();
		}

	});

});
