/**
 * This view is an example of how to use bi-directional data binding.  This example uses one of the
 * libs found here https://github.com/theironcook/Backbone.ModelBinder.  The page at that url has
 * example of how to instanciate, and different ways to bind DOM elements to data models. This example
 * specifically shares a model with bindingView.js and outputs the model in a readonly fashion to
 * demonstrate how a bound model can be shared across multiple views.
 */
define([
	'core',
	'models/binding',
	'text!html/tplBound.html',
	'modelBinder'
], function (core, BindingModel, template, ModelBinder) {

	mv.views.BoundView = Backbone.View.extend({

		el: '',

		//template: _.template(template),

		initialize: function () {
			if (!this.model) {
				this.model = new BindingModel();
			}
			this.modelBinder = new ModelBinder();
		},

		render: function () {
			this.$el.html(template);

			/**
			 * This is a mapping hash used to explicitly define the bi-directional mapping.  The
			 * key is the data model attribute, and the value is the DOM selector
			 */
			this.bindings = {
				'fName': '#fnamedisplay',
				'lName': '#lnamedisplay',
				'email': '#emaildisplay',
				'state': '#statedisplay',
				'gender': '#genderdisplay',
				'sendUpdates': '#send-updatesdisplay'
			};

			/**
			 * This binds the model to the view.
			 * Optional 3rd param is to provide explicit mappings.  Default will match element's "name" attribute to model attribute
			 * Optional 4rd param is for overriding global modelbinder options on an instance
			 */
			this.modelBinder.bind(this.model, this.el, this.bindings);
		},

		/**
		 * This method is triggered via the prototyped close function if this this view is closed
		 * with something like viewInstance.close()
		 */
		onClose: function () {
			this.modelBinder.unbind();
		}

	});

	return mv.views.BoundView;
});
