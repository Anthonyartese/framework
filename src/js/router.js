define([
	'core'
], function (core) {

	return Backbone.Router.extend({
		initialize: function () {},

		routes: {
			'': 'loadLandingPage',
			'real': 'loadLandingPage',
			':section(/*params)': 'loadSection'
		},

		loadLandingPage: function () {
			this.loadSection('home');
		},

		loadSection: function (sectionName, params) {
			var that = this;
			require(['presenters/' + sectionName], function (section) {
				try {
					section.load(params);
				} catch (error) {
					that._sectionLoadError(error);
				}

			}, this._sectionLoadError);
		},

		_sectionLoadError: function (error) {
			$(mv.sections.mainContainer).html('custom 404 page - probably best to load a template');
			mv.log('Routing error', 'err', error.stack);
		}

	});
});
