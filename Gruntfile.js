var JSON5 = require('json5');

module.exports = function (grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		SRC_PATH: './src/',
		BUILD_PATH: './target/',
		TEST_PATH: './test/',

		/**
		 * Used to remove files or directories
		 */
		clean: {
			target: ['<%= BUILD_PATH %>'] //build files
		},

		/**
		 * Used to copy files or directories
		 */
		copy: {
			target: {
				files: [{ // Copy source into build folder for further manipulation
					expand: true,
					cwd: '<%= SRC_PATH %>',
					src: ['**'],
					dest: '<%= BUILD_PATH %>'
				}]
			}
		},

		/**
		 * Used to minify css files, and takes any @import references and concats the into that same file
		 */
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: '<%= SRC_PATH %>' + 'css/',
					src: ['*.css'],
					dest: '<%= BUILD_PATH %>' + 'css/'
				}]
			}
		},

		/**
		 * Used to minify javascript files or directories.
		 * By not specifying a destination directory, it will overwrite the source file
		 */
		uglify: {
			target: {
				files: [{
					expand: true,
					src: [
						'<%= BUILD_PATH %>' + 'bower_components/requirejs/**/*.js'
					]
				}]
			}
		},

		/**
		 * The RequireJS plugin that will use uglify2 to build and minify our
		 * JavaScript, templates and any other data we include in the require files.
		 */
		requirejs: {
			compile: {
				options: JSON5.parse(grunt.file.read('require-build-config.json'))
			}
		},

		// Code formatting configuration
		jsbeautifier: {
			modify: {
				src: [
					'Gruntfile.js',
					'<%= SRC_PATH %>' + '**/*.js',
					'!<%= SRC_PATH %>' + 'lib/**/*.js', //exclude lib directory in src
					'!<%= SRC_PATH %>' + 'bower_components/**/*.js', //exclude lib directory in src
					'<%= TEST_PATH %>' + '/**/*.js',
					'!<%= TEST_PATH %>' + 'results/**/*.js' //eclude results directory in test
				],
				options: {
					config: '.jsbeautifyrc'
				}
			}
		},

		/**
		 * Code quality check configuration
		 */
		jshint: {
			all: ['<%= SRC_PATH %>' + '**/*.js'],
			options: {
				reporter: './jenkins_jslint_reporter_xml.js',
				reporterOutput: 'hintresults.xml',
				force: false, //to not fail the task if errors are encoutered
				jshintrc: '.jshintrc',
				ignores: [
					'<%= SRC_PATH %>' + 'bower_components/**/*.js',
					'<%= SRC_PATH %>' + 'lib/**/*.js'
				]
			}
		},

		/**
		 * Unit testing configuration
		 */
		karma: {
			options: {
				configFile: './test/karma.config.js'
				// browsers: [
				//     'Chrome',
				//     'Firefox'
				//      'IE'
				//      'Safari'
				//      'PhantomJS'
				//	]

			},
			validateWindows: {
				browsers: ['Chrome', 'Firefox', 'IE'],
				singleRun: true
			},
			validateMac: {
				browsers: ['Safari', 'Chrome', 'Firefox'],
				singleRun: true
			},
			validateIE: {
				browsers: ['IE'],
				singleRun: true
			},
			windowsDev: {
				browsers: ['Chrome', 'Firefox', 'IE'],
				autoWatch: true
			},
			macDev: {
				browsers: ['Safari', 'Chrome', 'Firefox'],
				autoWatch: true
			},
			deployValidate: {
				browsers: ['PhantomJS'],
				singleRun: true
			}
		},

		/**
		 * Auto documentation of your source code
		 */
		docker: {
			app: {
				expand: true,
				src: ['<%= SRC_PATH %>' + '**/*.js', '<%= SRC_PATH %>' + '**/*.css', '<%= SRC_PATH %>' + '**/*.html'],
				dest: 'doc',
				options: {
					onlyUpdated: false,
					colourScheme: 'default',
					ignoreHidden: false,
					sidebarState: true,
					exclude: '<%= SRC_PATH %>' + 'lib/**/*',
					lineNums: true,
					js: [],
					css: [],
					extras: ['fileSearch', 'goToLine']
				}
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-cssmin'); //css minification plugin
	grunt.loadNpmTasks('grunt-contrib-htmlmin'); //HTML minification plugin
	grunt.loadNpmTasks('grunt-contrib-uglify'); //JS minification
	grunt.loadNpmTasks('grunt-contrib-requirejs'); //RequireJS optimization
	grunt.loadNpmTasks('grunt-contrib-clean'); //empty directory contents
	grunt.loadNpmTasks('grunt-contrib-copy'); //copy directory contents
	grunt.loadNpmTasks('grunt-jsbeautifier'); //formatting code
	grunt.loadNpmTasks('grunt-contrib-jshint'); //for hinting code quality
	grunt.loadNpmTasks('grunt-karma'); //cross-browser execution of unit tests
	grunt.loadNpmTasks('grunt-docker'); //automated process to document src code

	// Default task.
	grunt.registerTask('default', 'show options', function () {
		grunt.log.writeln('\nThese are your common options:\n');
		grunt.log.writeln('validate  <-- ensures code quality before checking in by code formatting, JSHint, and unit tests\n');
		grunt.log.writeln('document  <-- document the source code\n');
		grunt.log.writeln('optimize  <-- bundles and minifies applicable static files for optimal page performance\n');
		grunt.log.writeln('karmaDev  <-- starts karma, runs unit tests, then stays open and listens for file changes\n');
		grunt.log.writeln('validateDeploy  <-- one last check for errors before deploying\n');
	});

	// Run this task to validate your code quality, code formatting, hinting & unit tests
	grunt.registerTask('validate', function () {
		grunt.log.writeln('isWindows');
		grunt.log.writeln(!!process.platform.match(/^win/));
		grunt.task.run(['jsbeautifier:modify']); //format the code
		grunt.task.run(['jshint']); //validate for coding error
		grunt.task.run(['karmaValidate']); //run the unit tests
	});

	// Run this task to generate documentation from the JSDoc in your code
	grunt.registerTask('document', function () {
		grunt.task.run(['docker']);
	});

	// Run this task to optimize your code for a production or production-like environments
	grunt.registerTask('optimize', function () {
		grunt.task.run(['clean:target']); //delete any previous optimizations
		grunt.task.run(['copy:target']); //copy src files to target
		grunt.task.run(['cssmin']); //concat and minify css
		grunt.task.run(['requirejs']); //optimize amd modules
		grunt.task.run(['uglify']); //minify JS files
	});

	// Trigger a one-time validation of files, based on operating system
	grunt.registerTask('karmaValidate', function () {
		if (isWindows()) {
			grunt.task.run(['karma:validateWindows']);
		} else {
			grunt.task.run(['karma:validateMac']);
		}
	});

	// Trigger continuous unit testing, based on operating system
	grunt.registerTask('karmaDev', function () {
		if (isWindows()) {
			grunt.task.run(['karma:windowsDev']);
		} else {
			grunt.task.run(['karma:macDev']);
		}
	});

	// Run this task before deploying to check for errors
	grunt.registerTask('validateDeploy', function () {
		grunt.task.run(['jshint']); //validate for coding error
		grunt.task.run(['karma:deployValidate']); // run unit tests
	});

	var isWindows = function () {
		return !!process.platform.match(/^win/);
	};

	grunt.registerTask('validate-ie', ['karma:validateIE']);
};
