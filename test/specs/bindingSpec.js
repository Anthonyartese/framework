define([
	'core',
	'views/bindingView',
	'test/helpers/basicFixture'
], function (core, BindingView) {

	describe('Binding view spec ', function () {
		var bindingView, type;

		beforeEach(function () {
			bindingView = new BindingView({
				el: '#testing-section'
			});
		});

		afterEach(function () {
			bindingView.close();
		});

		it('Test initial modelToView binding via render', function () {
			var expected = {
				'fName': 'Guy',
				'lName': 'Dude',
				'email': 'guy.dude@ge.com',
				'state': 'MI',
				'gender': 'male',
				'sendUpdates': true
			};

			bindingView.model.set(expected);
			bindingView.render();

			expect(bindingView.$('#fname').val()).toEqual(expected.fName);
			expect(bindingView.$('#lname').val()).toEqual(expected.lName);
			expect(bindingView.$('#email').val()).toEqual(expected.email);
			expect(bindingView.$('#state').val()).toEqual(expected.state);
			expect(bindingView.$('[name="gender"]').val()).toEqual(expected.gender);
			expect(bindingView.$('#send-updates').val()).toEqual('on');
			// expect(bindingView.$('#send-updates')).toBeChecked();
		});

		it('Test subsequent modelToView binding ', function () {
			var expected = 'Paul';
			bindingView.render();
			bindingView.model.set('fName', expected);
			var firstName = bindingView.$('#fname').val();
			expect(firstName).toEqual(expected);
		});

		it('Test the viewToModel ', function () {
			var expected = {
				'fName': 'Guy',
				'lName': 'Dude',
				'email': 'guy.dude@ge.com',
				'state': 'MI',
				'gender': 'male',
				'sendUpdates': true
			};
			bindingView.render();

			bindingView.$('#fname').val(expected.fName);
			bindingView.$('#lname').val(expected.lName);
			bindingView.$('#email').val(expected.email);
			bindingView.$('#state').val(expected.state);
			bindingView.$('[name="gender"]').val(expected.gender);
			bindingView.$('#fname, #lname, #email, #state, [name="gender"], #send-updates').trigger('change'); //trigger the events the modelBinder is listening for
			bindingView.$('#send-updates').trigger('click'); //simply trigger the selection of the checkbox

			expect(bindingView.model.get('fName')).toEqual(expected.fName);
			expect(bindingView.model.get('lName')).toEqual(expected.lName);
			expect(bindingView.model.get('email')).toEqual(expected.email);
			expect(bindingView.model.get('state')).toEqual(expected.state);
			expect(bindingView.model.get('gender')).toEqual(expected.gender);
			//expect(bindingView.model.get('sendUpdates')).toEqual(expected.sendUpdates);
		});

		it('Test validation against rules', function () {
			var expected = {
				'fName': 'Guy',
				'lName': 'Dude',
				'email': 'guy.invalidemail',
				'state': 'MI',
				'gender': 'male',
				'sendUpdates': true
			};

			bindingView.model.set(expected);
			bindingView.render();

			bindingView.model.validate();
			expect(bindingView.model.isValid()).toEqual(false);
			bindingView.model.set('email', 'guy.dude@ge.com');
			expect(bindingView.model.isValid()).toEqual(true);
		});

		it('Test validation callback', function () {
			var expected = {
				'fName': 'Guy',
				'lName': 'Dude',
				'email': 'guy.invalidemail',
				'state': 'MI',
				'gender': 'male',
				'sendUpdates': true
			};

			bindingView.model.set(expected);
			bindingView.render();

			bindingView.model.validate();
			expect($(bindingView.bindings['email']).closest('.form-group').hasClass('has-error')).toEqual(true);
			bindingView.model.set('email', 'guy.dude@ge.com');
			expect($(bindingView.bindings['email']).closest('.form-group').hasClass('has-success')).toEqual(true);
		});
	});

});
