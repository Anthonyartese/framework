define([
	'core',
	'util/logger'
], function (core) {

	/**
	 * These tests are configured to log and persist logs
	 */
	describe('Logging and persisting errors ', function () {

		var errorLiteralObj,
			jsError;

		beforeEach(function () {

			errorLiteralObj = {
				err: 'error'
			};

			try {
				var a;
				var b = a.toUpperCase();
			} catch (err) {
				jsError = err;
			}

			//log and persist all
			mv.env.logTypesToLog = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN, mv.enums.logTypes.INFO];
			mv.env.logTypesToPersist = [mv.enums.logTypes.ERROR, mv.enums.logTypes.WARN, mv.enums.logTypes.INFO];

			//spy on the ajax calls made in the logger module to persist
			spyOn($, 'ajax').and.callFake(function (req) {
				var d = $.Deferred();
				d.resolve();
				return d.promise();
			})
		});

		// without error objects
		it('Log and persist - log an info without object ', function () {
			var result = mv.log('hey', 'info');
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an error without object ', function () {
			var result = mv.log('hey', 'warn');
			expect(result.type).toEqual('warn');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an error without object ', function () {
			var result = mv.log('hey', 'err');
			expect(result.type).toEqual('err');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an invalid type without object ', function () {
			var result = mv.log('hey', 'sdfasd');
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		// with literal error objects
		it('Log and persist - log an info without object ', function () {
			var result = mv.log('hey', 'info', errorLiteralObj);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an error without object ', function () {
			var result = mv.log('hey', 'warn', errorLiteralObj);
			expect(result.type).toEqual('warn');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an error without object ', function () {
			var result = mv.log('hey', 'err', errorLiteralObj);
			expect(result.type).toEqual('err');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an invalid type without object ', function () {
			var result = mv.log('hey', 'sdfasd', errorLiteralObj);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		// with an actual js error
		it('Log and persist - log an info without object ', function () {
			var result = mv.log('hey', 'info', jsError);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an error without object ', function () {
			var result = mv.log('hey', 'warn', jsError);
			expect(result.type).toEqual('warn');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an error without object ', function () {
			var result = mv.log('hey', 'err', jsError);
			expect(result.type).toEqual('err');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

		it('Log and persist - log an invalid type without object ', function () {
			var result = mv.log('hey', 'sdfasd', jsError);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(true);
			expect(result.persisting).toBe(true);
		});

	});

	/**
	 * These tests are configured to not log our attempt to persist logs
	 */
	describe('Turn off logging and persisting of errors ', function () {

		var errorLiteralObj = {
				err: 'error'
			},
			jsError;

		beforeEach(function () {
			// create & catch actuall error
			try {
				var a;
				var b = a.toUpperCase();
			} catch (err) {
				jsError = err;
			}

			// turn off logging and persisting
			mv.env.logTypesToLog = [];
			mv.env.logTypesToPersist = [];
		});

		// without error objects
		it('no logging or persisting - log an info without object ', function () {
			var result = mv.log('hey', 'info');
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an error without object ', function () {
			var result = mv.log('hey', 'warn');
			expect(result.type).toEqual('warn');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an error without object ', function () {
			var result = mv.log('hey', 'err');
			expect(result.type).toEqual('err');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an invalid type without object ', function () {
			var result = mv.log('hey', 'sdfasd');
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		// with literal error objects
		it('no logging or persisting - log an info without object ', function () {
			var result = mv.log('hey', 'info', errorLiteralObj);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an error without object ', function () {
			var result = mv.log('hey', 'warn', errorLiteralObj);
			expect(result.type).toEqual('warn');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an error without object ', function () {
			var result = mv.log('hey', 'err', errorLiteralObj);
			expect(result.type).toEqual('err');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an invalid type without object ', function () {
			var result = mv.log('hey', 'sdfasd', errorLiteralObj);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		// with an actual js error
		it('no logging or persisting - log an info without object ', function () {
			var result = mv.log('hey', 'info', jsError);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an error without object ', function () {
			var result = mv.log('hey', 'warn', jsError);
			expect(result.type).toEqual('warn');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an error without object ', function () {
			var result = mv.log('hey', 'err', jsError);
			expect(result.type).toEqual('err');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

		it('no logging or persisting - log an invalid type without object ', function () {
			var result = mv.log('hey', 'sdfasd', jsError);
			expect(result.type).toEqual('info');
			expect(result.addedToLog).toBe(false);
			expect(result.persisting).toBe(false);
		});

	});

	/**
	 * Reset the environmental variables
	 */

	mv.env.setEnvironment();

});
