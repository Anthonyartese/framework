define([
	'core',
	'views/bindingView',
	'views/boundView',
	'text!html/regions/tplTwoColumns.html',
	'test/helpers/basicFixture'
], function (core, BindingView, BoundView, homeRegions) {

	describe('Binding view spec ', function () {
		var bindingView, boundView;

		beforeEach(function () {
			$('#testing-section').html(homeRegions);

			bindingView = new BindingView({
				'el': '#col1'
			});

			boundView = new BoundView({
				'el': '#col2',
				'model': bindingView.model
			});
		});

		afterEach(function () {
			bindingView.close();
			boundView.close();
		});

		it('Test binding of model across views', function () {
			var expected = {
				'fName': 'Guy',
				'lName': 'Dude',
				'email': 'guy.dude@ge.com',
				'state': 'MI',
				'gender': 'male',
				'sendUpdates': true
			};

			bindingView.model.set(expected);
			bindingView.render();
			boundView.render();

			bindingView.model.set('email', 'test@test.com');
			expect($(boundView.bindings['email']).text()).toEqual('test@test.com');
		});

	});

});
