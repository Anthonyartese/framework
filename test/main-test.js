/**
 * Launcher File.  Used to setup RequireJS and Start unit testing with Karma
 */

// the base url used by karma to identify files
var baseUrl = 'base/src/js';

// create an array of critical files for the require config dependency below
var defaultIncludes = [
	// 'environmentConfig'
];

// also add in the spec files themselves to run
for (var file in window.__karma__.files) {
	if (window.__karma__.files.hasOwnProperty(file)) {
		if (/Spec\.js$/.test(file)) {
			defaultIncludes.push(file);
		}
	}
}

require.config({
	baseUrl: baseUrl,
	paths: {
		//test only dependency
		'jasmine-jquery': '../bower_components/jasmine-jquery/lib/jasmine-jquery',

		//paths
		html: '../html',
		lib: '../lib',
		mocks: '../mocks',
		test: '../../test',
		locale: '../locale',

		//requirejs plugins
		text: '../bower_components/requirejs-text/text',
		json: '../bower_components/requirejs-json/json',
		i18n: '../bower_components/requirejs-i18n/i18n',

		//Bower dependencies
		jquery: '../bower_components/jquery/dist/jquery',
		underscore: '../bower_components/underscore/underscore',
		backbone: '../bower_components/backbone/backbone',
		bootstrap: '../bower_components/bootstrap/dist/js/bootstrap',
		highcharts: '../bower_components/highcharts/highcharts.src',
		modelBinder: '../bower_components/backbone.modelbinder/Backbone.ModelBinder',
		validation: '../bower_components/backbone.validation/dist/backbone-validation-amd',

		//other libs and components
		backboneGlobal: '../lib/backbone.global-0.1.2',
		highchartOptions: 'util/highchartOptions'
	},
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		bootstrap: {
			deps: ['jquery']
		},
		'jasmine-jquery': {
			deps: ['jquery']
		}
	},

	priority: [],
	// locale: 'en-us',
	urlArgs: 'bust=' + (new Date()).getTime(),

	// ask Require.js to load these files (all our tests)
	deps: defaultIncludes,

	// start test run, once Require.js is done
	callback: window.__karma__.start
});
